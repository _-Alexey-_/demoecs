﻿using DemoECS.Components;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using Random = Unity.Mathematics.Random;

namespace DemoECS
{
    public static class Utils
    {
        #region Fields

        private const float k_RotationRange = 3;

        #endregion Fields

        #region Properties

        public static Random Random = new Random((uint)UnityEngine.Random.Range(1, 100000));

        #endregion Properties

        #region Public Methods

        public static float3 GetRandomPosition()
        {
            float x = Random.NextFloat(GameManager.Settings.WorldWidth) - GameManager.Settings.HalfWorldWidth;
            float y = Random.NextFloat(GameManager.Settings.WorldHeight) - GameManager.Settings.HalfWorldHeight;
            return new float3(x, y, 0);
        }

        public static Color GetRandomPlayerColor() => Color.HSVToRGB(Random.NextFloat(), 0.7f, 1f);

        public static Color GetRandomFoodColor() => Color.HSVToRGB(Random.NextFloat(), 0.5f, 1f);

        public static RotationAngle GetRandomRotationAngle() => new RotationAngle { Value = Random.NextFloat(-k_RotationRange, k_RotationRange) };

        public static float3 GetMousePosition() => Camera.main.ScreenToWorldPoint(Input.mousePosition);

        public static Scale SizeToScale(Size size) => new Scale { Value = math.sqrt(size.Value) * Constants.SizeToScaleFactor };

        public static Speed SizeToSpeed(Size size) => new Speed { Value = Mathf.Lerp(Constants.MaxSpeed, Constants.MinSpeed, Mathf.InverseLerp(Constants.MinPlayerSize, Constants.MaxPlayerSize, size.Value)) };

        #endregion Public Methods
    }
}
