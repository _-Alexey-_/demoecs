﻿using Unity.Entities;
using Unity.Mathematics;

namespace DemoECS.Components
{
    public struct Food : IComponentData { }

    public struct Player : IComponentData { }

    public struct AIPlayer : IComponentData { }

    public struct RemoveEntity : IComponentData { }

    public struct MoveDirection : IComponentData
    {
        public float3 Value;
    }

    public struct Direction : IComponentData
    {
        public float3 Value;
    }

    public struct Size : IComponentData
    {
        public int Value;
    }

    public struct Speed : IComponentData
    {
        public float Value;
    }

    public struct RotationAngle : IComponentData
    {
        public float Value;
    }
}
