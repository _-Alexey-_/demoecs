﻿using DemoECS.Components;
using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace DemoECS
{
    public class FollowPlayer : MonoBehaviour
    {
        #region Fields

        [SerializeField]
        private Camera m_Camera = null;

        private const int k_MinPlayerSize = 50;
        private const float k_ScaleFactor = 3;

        private EntityQuery m_PlayersQuery;
        private EntityQuery m_AIPlayersQuery;
        private EntityManager m_EntityManager;
        private Entity m_TargetEntity = Entity.Null;

        #endregion Fields

        #region MonoBehavoiur Methods

        private void Start()
        {
            m_EntityManager = World.Active.EntityManager;

            m_PlayersQuery = m_EntityManager.CreateEntityQuery(typeof(Player), typeof(Translation), typeof(Size));
            m_AIPlayersQuery = m_EntityManager.CreateEntityQuery(typeof(AIPlayer), typeof(Translation), typeof(Size));
        }

        private void Update()
        {
            if (!m_EntityManager.Exists(m_TargetEntity) || !m_EntityManager.HasComponent<Translation>(m_TargetEntity))
                m_TargetEntity = Entity.Null;

            if (m_TargetEntity == Entity.Null)
                m_TargetEntity = FindTargetEntity();

            if (m_TargetEntity != Entity.Null)
            {
                transform.position = GetTargetPosition();

                var size = m_EntityManager.GetComponentData<Scale>(m_TargetEntity).Value;
                m_Camera.orthographicSize = Mathf.Lerp(m_Camera.orthographicSize, size * k_ScaleFactor, Time.deltaTime);
            }
        }

        #endregion MonoBehavoiur Methods

        #region Private Methods

        private Entity FindTargetEntity()
        {
            Entity target = Entity.Null;

            // Target player if exists
            NativeArray<Entity> players = m_PlayersQuery.ToEntityArray(Allocator.TempJob);
            if (players.Length > 0)
                target = players[0];
            players.Dispose();

            // Target the AI player otherwise
            if (target == Entity.Null)
            {
                players = m_AIPlayersQuery.ToEntityArray(Allocator.TempJob);
                NativeArray<Size> playersSizes = m_AIPlayersQuery.ToComponentDataArray<Size>(Allocator.TempJob);

                for (int i = 0; i < playersSizes.Length; i++)
                {
                    if (playersSizes[i].Value > k_MinPlayerSize)
                    {
                        target = players[i];
                        break;
                    }
                }

                players.Dispose();
                playersSizes.Dispose();
            }

            return target;
        }

        private Vector3 GetTargetPosition()
        {
            var pos = m_EntityManager.GetComponentData<Translation>(m_TargetEntity).Value;
            pos.z = -1;
            return Vector3.Lerp(transform.position, pos, Time.deltaTime);
        }

        #endregion Private Methods
    }
}
