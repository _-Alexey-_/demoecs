﻿using DemoECS.Components;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;

namespace DemoECS.Systems
{
    [UpdateAfter(typeof(CollisionSystem))]
    public class RemoveEntitySystem : JobComponentSystem
    {
        private struct RemoveAIPlayerJob : IJobForEachWithEntity<RemoveEntity, AIPlayer>
        {
            [WriteOnly] public EntityCommandBuffer.Concurrent CommandBuffer;

            public void Execute(Entity entity, int index, [ReadOnly] ref RemoveEntity rm, [ReadOnly] ref AIPlayer player)
            {
                Size size = new Size() { Value = Constants.InitialPlayerSize };
                CommandBuffer.SetComponent(index, entity, size);
                CommandBuffer.SetComponent(index, entity, Utils.SizeToScale(size));
                CommandBuffer.SetComponent(index, entity, new Speed { Value = Constants.InitialSpeed });
                CommandBuffer.SetComponent(index, entity, new Translation { Value = Utils.GetRandomPosition() });

                CommandBuffer.RemoveComponent(index, entity, typeof(RemoveEntity));
            }
        }

        private struct RemoveFoodJob : IJobForEachWithEntity<RemoveEntity, Food>
        {
            [WriteOnly] public EntityCommandBuffer.Concurrent CommandBuffer;

            public void Execute(Entity entity, int index, [ReadOnly] ref RemoveEntity rm, [ReadOnly] ref Food food)
            {
                CommandBuffer.SetComponent(index, entity, new Translation { Value = Utils.GetRandomPosition() });

                CommandBuffer.RemoveComponent(index, entity, typeof(RemoveEntity));
            }
        }

        private RemoveCommandBuffer m_CommandBuffer;

        protected override void OnCreate()
        {
            m_CommandBuffer = World.GetOrCreateSystem<RemoveCommandBuffer>();
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var palyerJob = new RemoveAIPlayerJob()
            {
                CommandBuffer = m_CommandBuffer.CreateCommandBuffer().ToConcurrent()
            }.Schedule(this, inputDeps);

            var foodJob = new RemoveFoodJob()
            {
                CommandBuffer = m_CommandBuffer.CreateCommandBuffer().ToConcurrent()
            }.Schedule(this, inputDeps);

            var jobs = JobHandle.CombineDependencies(palyerJob, foodJob);

            m_CommandBuffer.AddJobHandleForProducer(jobs);

            return jobs;
        }
    }
}
