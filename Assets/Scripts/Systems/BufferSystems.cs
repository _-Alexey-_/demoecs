﻿using Unity.Entities;

namespace DemoECS.Systems
{
    public class TargetAIPlayerBuffer : EntityCommandBufferSystem { }

    public class TargetPlayerBuffer : EntityCommandBufferSystem { }

    public class CollisionBuffer : EntityCommandBufferSystem { }

    public class MovementBuffer : EntityCommandBufferSystem { }

    public class RemoveCommandBuffer : EntityCommandBufferSystem { }
}
