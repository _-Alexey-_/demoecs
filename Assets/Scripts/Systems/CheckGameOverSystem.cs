﻿using DemoECS.Components;
using Unity.Entities;
using Unity.Rendering;
using Unity.Transforms;

namespace DemoECS.Systems
{
    [UpdateAfter(typeof(RemoveEntitySystem))]
    public class CheckGameOverSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            Entities.WithAll<Player, RemoveEntity>().ForEach((Entity entity) =>
            {
                GameManager.GameOver();

                PostUpdateCommands.RemoveComponent(entity, typeof(Player));
                PostUpdateCommands.RemoveComponent(entity, typeof(Translation));
                PostUpdateCommands.RemoveComponent(entity, typeof(RenderMesh));
            });
        }
    }
}
