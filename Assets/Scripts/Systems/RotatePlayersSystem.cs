﻿using DemoECS.Components;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace DemoECS.Systems
{
    [UpdateAfter(typeof(MovementSystem))]
    public class RotatePlayersSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            var forward = new float3(0, 0, 1);

            Entities.ForEach((ref Rotation rotation, ref RotationAngle angle) =>
            {
                rotation.Value = math.mul(rotation.Value, quaternion.AxisAngle(forward, angle.Value));
            });
        }
    }
}
