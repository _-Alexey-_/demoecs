﻿using DemoECS.Components;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

namespace DemoECS.Systems
{
    public class TargetAIPlayerSystem : JobComponentSystem
    {
        private struct TargetAIPlayerJob : IJobForEachWithEntity<AIPlayer, Translation, Size>
        {
            [WriteOnly] public EntityCommandBuffer.Concurrent CommandBuffer;
            [DeallocateOnJobCompletion] [ReadOnly] public NativeArray<Entity> Players;
            [DeallocateOnJobCompletion] [ReadOnly] public NativeArray<Translation> PlayersPositions;
            [DeallocateOnJobCompletion] [ReadOnly] public NativeArray<Size> PlayersSizes;
            [DeallocateOnJobCompletion] [ReadOnly] public NativeArray<Entity> Foods;
            [DeallocateOnJobCompletion] [ReadOnly] public NativeArray<Translation> FoodsPositions;
            [DeallocateOnJobCompletion] [ReadOnly] public NativeArray<Size> FoodsSizes;

            public void Execute(Entity entity, int index, [ReadOnly] ref AIPlayer player, [ReadOnly] ref Translation position, [ReadOnly] ref Size size)
            {
                float3? targetDir = null;
                float minDist = float.MaxValue;

                for (int i = 0; i < Players.Length; i++)
                {
                    var target = Players[i];
                    var targetPos = PlayersPositions[i];
                    var targetSize = PlayersSizes[i];

                    if (entity != target)
                    {
                        float d = math.length(position.Value - targetPos.Value);
                        if (d < minDist)
                        {
                            minDist = d;
                            targetDir = (size.Value > targetSize.Value ? 1 : -1) * (targetPos.Value - position.Value);
                        }
                    }
                }

                if (minDist > k_MinPlayerDistance)
                {
                    foreach (var targetPos in FoodsPositions)
                    {
                        float d = math.length(position.Value - targetPos.Value);
                        if (d < minDist)
                        {
                            minDist = d;
                            targetDir = targetPos.Value - position.Value;
                        }
                    }
                }

                if (targetDir.HasValue)
                    CommandBuffer.AddComponent(index, entity, new MoveDirection { Value = targetDir.Value });
            }
        }

        private const float k_MinPlayerDistance = Constants.MinPlayerSize * 5;

        private TargetAIPlayerBuffer m_CommandBuffer;
        private EntityQuery m_Players;
        private EntityQuery m_Foods;

        protected override void OnCreate()
        {
            m_CommandBuffer = World.GetOrCreateSystem<TargetAIPlayerBuffer>();

            m_Players = GetEntityQuery(new EntityQueryDesc
            {
                Any = new ComponentType[] { typeof(Player), typeof(AIPlayer) },
                All = new ComponentType[] { typeof(Translation), typeof(Size) }
            });

            m_Foods = GetEntityQuery(typeof(Food), typeof(Translation), typeof(Size));
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var job = new TargetAIPlayerJob
            {
                CommandBuffer = m_CommandBuffer.CreateCommandBuffer().ToConcurrent(),

                Players = m_Players.ToEntityArray(Allocator.TempJob),
                PlayersPositions = m_Players.ToComponentDataArray<Translation>(Allocator.TempJob),
                PlayersSizes = m_Players.ToComponentDataArray<Size>(Allocator.TempJob),

                Foods = m_Foods.ToEntityArray(Allocator.TempJob),
                FoodsPositions = m_Foods.ToComponentDataArray<Translation>(Allocator.TempJob),
                FoodsSizes = m_Foods.ToComponentDataArray<Size>(Allocator.TempJob)
            }.Schedule(this, inputDeps);

            m_CommandBuffer.AddJobHandleForProducer(job);

            return job;
        }
    }
}
