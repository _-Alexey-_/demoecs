﻿using DemoECS.Components;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

namespace DemoECS.Systems
{
    public class TargetPlayerSystem : JobComponentSystem
    {
        private struct TargetPlayerJob : IJobForEachWithEntity<Player, Translation>
        {
            [WriteOnly] public EntityCommandBuffer.Concurrent CommandBuffer;
            [ReadOnly] public float3 MousePosition;

            public void Execute(Entity entity, int index, [ReadOnly] ref Player player, [ReadOnly] ref Translation position)
            {
                float3 dir = MousePosition - position.Value;
                dir.z = 0;

                CommandBuffer.AddComponent(index, entity, new MoveDirection { Value = dir });
            }
        }

        private TargetPlayerBuffer m_CommandBuffer;

        protected override void OnCreate()
        {
            m_CommandBuffer = World.GetOrCreateSystem<TargetPlayerBuffer>();
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var job = new TargetPlayerJob
            {
                CommandBuffer = m_CommandBuffer.CreateCommandBuffer().ToConcurrent(),
                MousePosition = Utils.GetMousePosition()
            }.Schedule(this, inputDeps);

            m_CommandBuffer.AddJobHandleForProducer(job);

            return job;
        }
    }
}
