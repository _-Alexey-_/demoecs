﻿using DemoECS.Components;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace DemoECS.Systems
{
    [UpdateAfter(typeof(TargetAIPlayerSystem))]
    [UpdateAfter(typeof(TargetPlayerSystem))]
    public class MovementSystem : JobComponentSystem
    {
        private struct MovementJob : IJobForEachWithEntity<Translation, MoveDirection, Direction, Speed>
        {
            [WriteOnly] public EntityCommandBuffer.Concurrent CommandBuffer;
            [ReadOnly] public float Time;

            public void Execute(Entity entity, int index, ref Translation position, [ReadOnly] ref MoveDirection moveDirection, ref Direction direction, [ReadOnly] ref Speed speed)
            {
                if (Vector3.Angle(direction.Value, moveDirection.Value) > k_AllowedTurnAngle)
                {
                    var angle = Vector3.SignedAngle(direction.Value, moveDirection.Value, Vector3.back);
                    direction.Value = RotateVectorByAngle(direction.Value, angle);
                }
                else
                    direction.Value = math.normalize(moveDirection.Value);

                position.Value += direction.Value * speed.Value * Time;

                CommandBuffer.RemoveComponent(index, entity, typeof(MoveDirection));
            }
        }

        private MovementBuffer m_CommandBuffer;

        protected override void OnCreate()
        {
            m_CommandBuffer = World.GetOrCreateSystem<MovementBuffer>();
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var job = new MovementJob
            {
                CommandBuffer = m_CommandBuffer.CreateCommandBuffer().ToConcurrent(),
                Time = Time.deltaTime
            }.Schedule(this, inputDeps);

            m_CommandBuffer.AddJobHandleForProducer(job);

            return job;
        }


        private const float k_AllowedTurnAngle = 5f;
        private static float3 RotateVectorByAngle(float3 vector, float angle)
        {
            angle = Mathf.Clamp(angle, -k_AllowedTurnAngle, k_AllowedTurnAngle) * Mathf.Deg2Rad;

            return new Vector3(vector.x * Mathf.Cos(angle) + vector.y * Mathf.Sin(angle),
                              -vector.x * Mathf.Sin(angle) + vector.y * Mathf.Cos(angle)).normalized;
        }
    }
}
