﻿using DemoECS.Components;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

namespace DemoECS.Systems
{
    [UpdateAfter(typeof(CheckGameBoundsSystem))]
    public class CollisionSystem : JobComponentSystem
    {
        private struct CollisionJob : IJobForEachWithEntity<Translation, Scale, Size, Speed>
        {
            [WriteOnly] public EntityCommandBuffer.Concurrent CommandBuffer;
            [DeallocateOnJobCompletion] [ReadOnly] public NativeArray<Entity> Entities;
            [DeallocateOnJobCompletion] [ReadOnly] public NativeArray<Translation> Positions;
            [DeallocateOnJobCompletion] [ReadOnly] public NativeArray<Scale> Scales;
            [DeallocateOnJobCompletion] [ReadOnly] public NativeArray<Size> Sizes;

            public void Execute(Entity entity, int index, [ReadOnly] ref Translation pos, ref Scale scale, ref Size size, ref Speed speed)
            {
                for (int i = 0; i < Entities.Length; i++)
                {
                    var posOther = Positions[i].Value;
                    var scaleOther = Scales[i].Value;
                    var sizeOther = Sizes[i];

                    if (entity != Entities[i] && math.distance(pos.Value, posOther) < (scale.Value + scaleOther) / 2 && size.Value > sizeOther.Value)
                    {
                        size.Value += sizeOther.Value;
                        scale = Utils.SizeToScale(size);
                        speed = Utils.SizeToSpeed(size);
                        CommandBuffer.AddComponent(index, Entities[i], new RemoveEntity());
                    }
                }
            }
        }

        private CollisionBuffer m_CommandBuffer;
        private EntityQuery m_Entities;

        protected override void OnCreate()
        {
            m_CommandBuffer = World.GetOrCreateSystem<CollisionBuffer>();
            m_Entities = GetEntityQuery(typeof(Translation), typeof(Scale), typeof(Size));
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var job = new CollisionJob
            {
                CommandBuffer = m_CommandBuffer.CreateCommandBuffer().ToConcurrent(),
                Entities = m_Entities.ToEntityArray(Allocator.TempJob),
                Positions = m_Entities.ToComponentDataArray<Translation>(Allocator.TempJob),
                Scales = m_Entities.ToComponentDataArray<Scale>(Allocator.TempJob),
                Sizes = m_Entities.ToComponentDataArray<Size>(Allocator.TempJob)
            }.Schedule(this, inputDeps);

            m_CommandBuffer.AddJobHandleForProducer(job);

            return job;
        }
    }
}
