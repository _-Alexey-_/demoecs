﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace DemoECS.Systems
{
    [UpdateAfter(typeof(MovementSystem))]
    public class CheckGameBoundsSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            int halfWorldWidth = GameManager.Settings.HalfWorldWidth;
            int halfWorldHeight = GameManager.Settings.HalfWorldHeight;

            Entities.ForEach((ref Translation pos, ref Scale scale) =>
            {
                var halfScale = scale.Value / 2;

                pos.Value = new float3(
                    math.clamp(pos.Value.x, -halfWorldWidth + halfScale, halfWorldWidth - halfScale),
                    math.clamp(pos.Value.y, -halfWorldHeight + halfScale, halfWorldHeight - halfScale),
                    pos.Value.z);
            });
        }
    }
}
