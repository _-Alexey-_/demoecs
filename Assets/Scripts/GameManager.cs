﻿using DemoECS.Components;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;

namespace DemoECS
{
    public class GameManager : MonoBehaviour
    {
        #region Fields

        private static GameManager s_Instance = null;

        [Header("UI")]
        [SerializeField]
        private GameObject m_UIStartPanel = null;

        [Header("Prefabs")]
        [SerializeField]
        private Mesh m_Mesh = null;
        [SerializeField]
        private Material m_PlayerMaterial = null;
        [SerializeField]
        private Material m_AIPlayerMaterial = null;
        [SerializeField]
        private Material m_FoodMaterial = null;

        [Header("Settings")]
        [SerializeField]
        private Settings m_Settings;

        private EntityManager m_EntityManager;
        private EntityArchetype m_PlayerArchetype;
        private EntityArchetype m_AIPlayerArchetype;
        private EntityArchetype m_FoodArchetype;
        private Material[] m_PlayerMaterials;
        private Material[] m_FoodMaterials;

        #endregion Fields

        #region Properties

        public static Settings Settings { get; private set; }

        #endregion Properties

        #region MonoBehaviour Methods

        private void Awake()
        {
            s_Instance = this;

            m_EntityManager = World.Active.EntityManager;

            m_PlayerArchetype = m_EntityManager.CreateArchetype(
                typeof(Player),
                typeof(RenderMesh),
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(Rotation),
                typeof(Scale),
                typeof(Size),
                typeof(Speed),
                typeof(Direction),
                typeof(RotationAngle));

            m_AIPlayerArchetype = m_EntityManager.CreateArchetype(
                typeof(AIPlayer),
                typeof(RenderMesh),
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(Rotation),
                typeof(Scale),
                typeof(Size),
                typeof(Speed),
                typeof(Direction),
                typeof(RotationAngle));

            m_FoodArchetype = m_EntityManager.CreateArchetype(
                typeof(Food),
                typeof(RenderMesh),
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(Scale),
                typeof(Size));

            FillMaterialArrays();

            Settings = m_Settings;
        }

        #endregion MonoBehaviour Methods

        #region Public Methods

        public void StartGame()
        {
            DeleteAllEntities();

            SpawnFoods();
            SpawnAIPlayers();
            SpawnPlayer();

            // Hide start panel
            m_UIStartPanel.SetActive(false);
        }

        public static void GameOver()
        {
            // Show start panel
            GetInstance().m_UIStartPanel.SetActive(true);
        }

        #endregion Public Methods

        #region Private Methods
        private static GameManager GetInstance()
        {
            if (s_Instance == null)
                s_Instance = FindObjectOfType<GameManager>();

            return s_Instance;
        }

        private void DeleteAllEntities()
        {
            var entities = m_EntityManager.GetAllEntities();
            m_EntityManager.DestroyEntity(entities);
            entities.Dispose();
        }

        private void SpawnFoods()
        {
            for (int i = 0; i < Settings.PlayersCount; i++)
                SpawnFood(i);
        }

        private void SpawnAIPlayers()
        {
            for (int i = 0; i < Settings.PlayersCount; i++)
                SpawnAIPlayer(i);
        }

        private void SpawnPlayer()
        {
            var player = m_EntityManager.CreateEntity(m_PlayerArchetype);

            m_EntityManager.SetSharedComponentData(player, new RenderMesh
            {
                mesh = m_Mesh,
                material = m_PlayerMaterial
            });

            Size size = new Size() { Value = Constants.InitialPlayerSize };
            m_EntityManager.SetComponentData(player, size);
            m_EntityManager.SetComponentData(player, Utils.SizeToScale(size));
            m_EntityManager.SetComponentData(player, Utils.GetRandomRotationAngle());
            m_EntityManager.SetComponentData(player, new Speed { Value = Constants.InitialSpeed });
            m_EntityManager.SetComponentData(player, new Direction { Value = float3.zero });

            m_EntityManager.SetName(player, nameof(Player));
        }

        private void SpawnAIPlayer(int idx)
        {
            var player = m_EntityManager.CreateEntity(m_AIPlayerArchetype);

            m_EntityManager.SetSharedComponentData(player, new RenderMesh
            {
                mesh = m_Mesh,
                material = GetRandomPlayerMaterial()
            });

            Size size = new Size() { Value = Constants.InitialPlayerSize };
            m_EntityManager.SetComponentData(player, size);
            m_EntityManager.SetComponentData(player, Utils.SizeToScale(size));
            m_EntityManager.SetComponentData(player, Utils.GetRandomRotationAngle());
            m_EntityManager.SetComponentData(player, new Speed { Value = Constants.InitialSpeed });
            m_EntityManager.SetComponentData(player, new Translation { Value = Utils.GetRandomPosition() });
            m_EntityManager.SetComponentData(player, new Direction { Value = float3.zero });

            m_EntityManager.SetName(player, $"{nameof(AIPlayer)} {idx}");
        }

        private void SpawnFood(int idx)
        {
            var food = m_EntityManager.CreateEntity(m_FoodArchetype);

            m_EntityManager.SetSharedComponentData(food, new RenderMesh
            {
                mesh = m_Mesh,
                material = GetRandomFoodMaterial()
            });

            Size size = new Size() { Value = Constants.InitialFoodSize };
            m_EntityManager.SetComponentData(food, size);
            m_EntityManager.SetComponentData(food, Utils.SizeToScale(size));
            m_EntityManager.SetComponentData(food, new Translation { Value = Utils.GetRandomPosition() });

            m_EntityManager.SetName(food, $"{nameof(Food)} {idx}");
        }

        private void FillMaterialArrays()
        {
            m_PlayerMaterials = new Material[Constants.MaterialsCount];
            m_FoodMaterials = new Material[Constants.MaterialsCount];
            for (int i = 0; i < Constants.MaterialsCount; i++)
            {
                m_PlayerMaterials[i] = new Material(m_AIPlayerMaterial)
                {
                    color = Utils.GetRandomPlayerColor()
                };

                m_FoodMaterials[i] = new Material(m_FoodMaterial)
                {
                    color = Utils.GetRandomFoodColor()
                };
            }
        }

        private Material GetRandomPlayerMaterial()
        {
            return m_PlayerMaterials[Utils.Random.NextInt(Constants.MaterialsCount)];
        }

        private Material GetRandomFoodMaterial()
        {
            return m_FoodMaterials[Utils.Random.NextInt(Constants.MaterialsCount)];
        }

        #endregion Private Methods
    }
}